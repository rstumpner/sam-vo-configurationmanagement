<!-- $size: 16:9 -->
<!-- page_number: true -->
<!-- footer: Roland Stumpner GPLv3 -->




## Systemadministration
#### Vorlesung

---
## Konfigurationsmanagement
#### The Art of Deploying IT Services

---
## Agenda
* Deploying IT Services

* Anforderungen an die IT
* Aufgaben der IT in einem Unternehmen  
* Konzept as a Service
* Erstellen eines IT Servies
* Komponenten eines IT Services
* Lifecycle of an IT Service
* Traditionelle IT / Devops
* Deployment of a Webservice Beispiel
* Deployment Tools

---

## Deploying IT Services (Architektur)
* Infrastruktur
* Betriebssystem
* Packages / Libarys
* Installation des Programms / Sourcecode
* Nutzerdaten

---
## Infrastruktur
* Compute
* Storage
* Network

---
## Infrastruktur
* Hardware
* Virtual Machine
* Container
---
## Infrastruktur
(Deployment Methode)
* Manual
* Infrastruktur as Code

---
## Beispiel Container
(Dokumentation/Manual)
![Proxmox Container](_images/sam-vlab-proxmox-create-lxc-1.png)

---
## Betriebssystem
* Linux
* BSD
* Windows

---
## Betriebssystem
* Manual
* Scripted Installation
* BasisImage
* Template
---
## Beispiel Kickstart
```Kickstart

# Install, not upgrade
install

# Install from a friendly mirror and add updates
url --url=http://fedora.mirror.lstn.net/releases/17/Fedora/x86_64/os/
repo --name=updates

# Language and keyboard setup
lang en_US.UTF-8
keyboard us

# Configure DHCP networking w/optional IPv6, firewall on
network --onboot yes --device eth0 --bootproto dhcp --ipv6 auto --hostname fedora.local
firewall --service=ssh

# Services running at boot
services --enabled network,sshd
services --disabled sendmail

# Setup the disk
zerombr
clearpart --all --drives=xvda
part / --fstype=ext4 --grow --size=1024 --asprimary
part swap --size=512
bootloader --location=none --timeout=5 --driveorder=xvda

# Minimal package set
%packages --excludedocs
man
man-pages
sendmail
yum-presto
yum-updatesd
%end
```
---
## Packages
* Programmbibliotheken
* Schnittstellen
* Backendservices

---
## Packages
* Sourcecode
* Binary
* Package

---
## Packages
* Paketverwaltung
* Konfiguration

---
## Beispiel Paketinstallation

* apt-get install apache2

---
## Beispiel Apache Konfiguration

```ini
<VirtualHost *:80>
  DocumentRoot /opt/librenms/html/
  ServerName  librenms.example.com

  AllowEncodedSlashes NoDecode
  <Directory "/opt/librenms/html/">
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews
  </Directory>
</VirtualHost>
```

---
## Installation der Anwendung
(Webanwendung)

* Sourcecode
* Binary
* Paketverwaltung

---
## Installation Anwendung
* Sourcecode
* Konfiguration

---
## Beispiel Sourcecode
(Kanboard)

wget https://github.com/kanboard/kanboard/archive/v1.2.9.zip

---
## Beispiel Konfiguration
(Kanboard)
```php
//config.php
<?php
define('DB_RUN_MIGRATIONS', true);
// Database driver: sqlite, mysql or postgres (sqlite by default)
define('DB_DRIVER', 'sqlite');
// Mysql/Postgres username
define('DB_USERNAME', 'root');
// Mysql/Postgres password
define('DB_PASSWORD', '');
// Mysql/Postgres hostname
define('DB_HOSTNAME', 'localhost');
// Mysql/Postgres database name
define('DB_NAME', 'kanboard');
// Mysql/Postgres custom port (null = default port)
define('DB_PORT', null);
// Mysql SSL key
?>
```

---
## Nutzerdaten
(State)

* Files
* Datenbank

---
## Nutzerdaten Beispiele
* JSON
* SQLlite
* MariaDB
* MongoDB

---
## Konzept Technologiestack / Plattform
* Kombination an Services der ein Unterbau für eine Software ist
* Standing on the Sholders of Giants (OOS Prinzip)

Beispiel:
* LAMP

---
## Webstack Aufbau
(Beispiel LAMP)

| Nutzer| Daten |
| --- | --- |
| Package (Schnittstelle) | Apache
| Anwendung (Sourcecode) | PHP
| Package (Datenbank) | libsqlite
| Betriebssystem | Linux
| Infrastruktur | VM

---
## Es lebt ... Change
(Analyse)

---
## Infrastruktur (change)
* Hardware Erneuerung (Livecycle)
* Resource Upgrade
* VM Migration
* Security Updates

---
# Frage
### Auswirkungen Anwendung / Konfiguration

---
## Zeitfenster
#### Monate / Jahre

---
## Beispiele
* Firmware Update (Hersteller)
* Meltdown / Spectre

---
## Betriebssystem (change)
* Security Updates
* Versionsupdate

---
# Frage
### Auswirkungen Anwendung / Konfiguration

---
## Zeitfenster
#### Monate / Jahre

---
## Zeitfenster
#### Monate / Jahre
* sehr Statisch (gestern)
* dynamischer (Patchday)

---
## Beispiele
* Windows Update Manager / WSUS (Hersteller)
* Linux / BSD Updates (Distribution)

---
## Packages
* Security Updates
* Versionsupdates

---
# Frage
### Auswirkungen Anwendung / Konfiguration

---
## Zeitfenster
#### Wochen / Monate

---
## Zeitfenster
#### Wochen / Monate
* sehr Statisch (gestern)
* dynamischer (Patchday)

---
## Beispiele
* Windows Update Manager / WSUS (Repository)
  * nuget
  * Chocolate
* Linux / BSD Distribution Updates (Repository)

---
## Packages - Konfiguration
* Schnittstellenänderungen

---
# Frage
### Auswirkungen Anwendung / Konfiguration

---
## Zeitfenster
#### Monate / Jahre

---
## Anwendung / Sourcecode

* Bugfixes
* Security Updates
* Feature Updates

---
# Frage
### Auswirkungen Anwendung / Konfiguration

---
## Zeitfenster
#### Monate / Jahre

---
## Beispiele
* Manual
* Paketverwaltung
* Versionsverwaltung


---
## Sourcecode / Konfiguration
* Serviceänderungen

---
# Frage
### Auswirkungen Anwendung / Konfiguration

---
## Zeitfenster
#### Wochen / Monate

---
## Team

---
## Beispiele:
* Manual
* Versionsverwaltung

---
## Nutzerdaten
* Benutzereingabe

---
# Frage
### Auswirkungen Anwendung / Konfiguration

---
## Zeitfenster
#### Minuten / Stunden /Tag

---
## Team

---
## Beispiele
* Transaktionen
* Backups

---
## Es lebt ... Change
| Infrastruktur | Betriebssystem | Packages | Programme/Sourcecode | Nutzerdaten |
| --- | --- | --- | --- | --- |
| Statisch | Statisch | Dynamisch | sehr Dynamisch | noch Dynamischer |
| Livecycle | Security | Security + Features| Features | Kunde |
| 3 - 5 J| 3 - 4 M | 1 M | 2 W | 1 S |
| intern | extern | extern | extern (OPs) | extern |

---
## Wasserfallmodell
* seit den 1950ern
* sequenzielle Abarbeitung
* Anforderungen sind sehr Statisch
* Softwareentwicklung

---
## Wasserfallmodell

![Wasserfallmodell](_images/wasserfallmodell.png)

---
## Lebenszyklus einer Software (Wasserfallmodell)
* Anforderungen durch den Kunden
* Planung / Systemarchitektur
* Entwicklung / Installation
* Deployment Testsystem
* Deployment Produktion
* Abnahme

---
## Betrieb
* Betrieb und Wartung
* Dienst wird Eingestellt , nicht mehr benötigt
  * Abbau

---
## Lifecycle of an IT Service

![lifecycle old](_images/lifecycle.png)

---
# ITIL
##### (IT Infrastrukture Libary)
Ist eine Sammlung von Prozessen , Funktionen und Rollen im Umfeld von IT Services. Aus einem Stück Software wird ein IT Service.

* Grundsätzlich in den 1990ern bei den Britischen Behörden entstanden.
* 2001 ITILv2
* 2007 ITILv3
* ITSM
  * IT Service Management
* COBIT
* ISO 27001


---
## Deployment Werkzeuge
* Betriebssystem Images / Templates
* Repository
* Paketverwaltung
* Dokumentation
* Versionsverwaltung

---
## Betriebssystem Image / Templates

Ist eine Installation eines Betriebssystems welche die Systemparameter des Unternehmens enthält , und als Basis für die Installation der Applikation dient.  

---
## Beispiel:

* Vagrant
* Cloudimages
* Packer.io

---
## Repository
Eine Quelle für Daten die Zentral Verwaltet und Remote Verfügbar ist.

* Versionierung
* Signieren
* Verschlüsselung von Geheimnissen ( Username / Passwörter / Zertifikate)

---
## Beispiele
* OS Images
* Container
* Packages
* Konfiguration
* Sourcecode

---
## Paketverwaltung

Ein vom Hersteller oder der IT zur Verfügung gestelltes Format einer Software die Binary Dateien und ihre Installationsscripts enthält.
Mit einer Paketverwaltung kann über ein sog. Repository die Installation oder ein Update eines Programmpaketes sehr einfach und sicher gestaltet werden.

---
## Paketverwaltung Eigenschaften
* Versioniert
* Signiert
* Abhängigkeiten

#### Beispiele:
* Apt-get
* Nu-get
* Chocolate
* MSI
* Zypper
* yum


---
## Dokumentation

* Aktuell
* Umfassend
* Nachvollziehbar
* Versioniert
* ITILv3 Kompatibel

---
## Deploying IT Services Versioniert
* Durch ständige Änderungen an der Konfigurationsdatei des Dienstes verliert man den Überblick
* Abhilfe können Versionskontrollsysteme schaffen
* Bekannt aus der Programmierung
* Configuration Tracking

---
## Versionsverwaltung
(Versionskontrollsystem)
* Erfasst Änderungen in Dateien und Speichert diese mit Zeitstempel in einem Archivsystem. Daraus können diese Versionen eines Dokumentes wiederhergestellt werden.
* Findet in der Softwareentwicklung Verwendung aber zunehmend auch in Büroanwendungen und Content Management Systemen sowie im Change Management von IT Services

---
## Versionsverwaltung Aufgaben
* Protokollierung der Änderungen
* Wiederherstellen alter Versionen
* Archivierung von Veränderungen
* Gleichzeitige Bearbeitung von mehreren Versionen (Branches)

---
## Versionsverwaltung
(Lokal = Historisch )
#### Es wird nur die Veränderung einer Datei oder eines Verzeichnisses erfasst und lokal gespeichert.

---
## Beispiele
* Revision Control System
  * (https://de.wikipedia.org/wiki/Revision_Control_System)
* Source Code Control System
  * https://de.wikipedia.org/wiki/Source_Code_Control_System

---
## Versionsverwaltung
(Zentral)

Die Versionsgeschichte der Daten ist in einem sogenannten Repository am Server gespeichert und der Client legt sich eine Lokale Arbeitskopie an die mit dem Zentralen Repository abgeglichen werden muss.

---
## Arbeitsweise
Create -> Checkout -> Change <- Checkin
![Zentrale Versionsverwaltung]()

---
## Beispiele
* CVS
* SVN
* MS SourceSafe

---
## CVS
* Seit 1990
* Concurrent Versions System
* Weiterentwicklung von RCS (nur Dateien)
* Problem ist das Verschieben von Verzeichnissen

---
## SVN
* Seit 2000
* Apache Subversion
* Offen
* Weiterentwicklung von CVS
* CLI ähnlich CVS

---
## Versionsverwaltung
(Verteilt)

Jeder Mitarbeiter hat ein lokales Repository mit der kompletten Versionsgeschichte gespeichert und kann diese mit jedem anderen Teilnehmer abgleichen .
Bei einem Ablgeich kommt es nicht zu einem Konflikt sondern einfach zu einer neuen Version (Merge)

---
## Arbeitsweise
Clone / Change / Commit / Push
![Zentrale Versionsverwaltung]()

---
## Beispiele
* GIT (https://git-scm.com/)
* Bazaar
* Mercurial
* Bitkeeper
* bitbucket (https://bitbucket.org/)

---
## GIT
* Seit 2005
* Verteilte Versionsverwaltung
* Offen
* Speziell für nicht lineare Versionierung Entwickelt
* kein Zentraler Server nötig
* Jeder Benutzer Besitzt eine lokale Kopie des Repos

---
## GIT Cheatsheet
* Init
* Clone
* Commit
* Push
* Pull


---
## Konfigurationsmanagement

* Dokumentation aller Vorgänge
* Definition von Resourcen (Prozesse/Dateien)
* Versionierung
* Zugriffskontrolle
* Integration aller vorhandenen Werkzeuge
* Effizienzsteigerungen bei der automatisierten Applikationserstellung

---
## Desired State Configuration
* Zustandsorientierte Service Konfiguration
* Anforderungsliste an das System (Wunschliste) (Domain Specific Language)
  * Universell / nicht Systemabhängig
  * Anpassung / Compilieren für das Zielsystem (Server Orientiert / Agent Orientiert)
* Ausführen der Systemanpassungen um den geforderten Zustand der Anforderungsliste zu erreichen (make it so)
* Solange bis der Zustand erreicht ist

---
## Agentless Configuration
* Controller Node
* Standard Übertragunsprotokolle (SSH / WinRM)
* Standard Auth Mechanismen (Username/Passwort/ Zertifikate)

---
## Ad-hoc Task Based Configuration
* Ansible
* Pyinfra

---
## Template Based Configuration (Environments)
Es werden Templates mit Platzhalter erstellt und diese Platzhalter werden dann mit Systemspezifischen Konfiguratiosparameter befüllt
* Jinja2
* Puppet Template Config
  * ERP
  * EPP

---
## Konfigurationsmanagement
(Beispiele)
* Cfengine
* Puppet
* Chef
* Ansible
* SALT
* Powershell DSC
* MS Group Policy

---
## cfengine
* 1993
* Regelbasiertes Computerverwaltungssystem
* OpenSource
* Automatisiert
* Gruppierung
* OS Unabhängig (UNIX)
* Richtlinien-kompatibler Systemzustand
* Desired State Configuration

---
## Puppet
* 2005
* Desired State Configuration
* Open Source
* Einzelplatz sowie Serverlösung
* Über Templates wird ein Endzustand des Systems festgelegt
* Der Puppetagent setzt dieses Template dann um
* OS Unabhängig (Unix / Windows / Switches)
* Domain Specific Language
* Manifests
* Nodes
* Class
* Server Compile

---
## Chef
* 2008
* OpenSource
* Desired State Configuration
* Policy Based
* Client Server Anwendung (Spezialconfig für single Server)
* OS Unabhängig (Unix / Windows / Switches)
* Nodes
* Roles
* Environments
* Organisations
* Cookbooks
* Recipes
* Client Compile

---
## Ansible
* 2012
* OpenSource
* YAML + Variablen = Configdatei
* Playbook
* No Agent
* Connecting via SSH and WinRM
* Execute the Modules
* Delete and go

---
## JuJu
* 2012
* OpenSource
* Ubuntu
* MAAS
* Openstack
* Orchestration
* Charms
* Hooks
* YAML
* And Domain Specific Language

---
# SALT
* 2012
* OpenSource
* YAML + Variablen = Configdatei

---
## Powershell DSC
* 2012
* Microsoft
* Desired State Configuration
* Policy Based

---
## System Center Configuration Manager
* 2007
* Microsoft
* Desired State Configuration
* Policy Based

---
## Summary Configuration Management

![](_images/docker-architektur.png)

---
## SAM Vorlesung Softwaredeployment

---
## Webapplikation / Architektur
#### (Statisch)

* HTML
* Javascript
* Bilder

---
## Plattform / Stack

| Webserver | Apache |
| --- | --- |
| Anwendung/Sourcecode | index.html |
| Betriebssystem | Linux |
| Infrastruktur | VM |


---
## Entwicklung
#### Editor

```
<!DOCTYPE html>
<html>
<head>
<title>SAM Vorlesung</title>
</head>
<body>

<h1>Hello from SAM </h1>

</body>
</html>
```

---
## Testumgebung
* Browser http://localhost/index.html

---
## Deployment
* rsync
* ftp

---
## Produktivumgebung
#### Plattform / Stack

| Webserver | Apache |
| --- | --- |
| Anwendung/Sourcecode | index.html |
| Betriebssystem | Linux |
| Infrastruktur | VM |

---
## Test / Abnahme
* Browser http://produktiv.sam.at/index.html

---
## Management
* Wasserfallmodell

---
## Verbesserung Wartbarkeit / Inhalt
* Static Site Generators

---
## Static Site Generators

Aus statischem Inhalt (meist) Textdateien (Markdown) und einem Template wird ein Statischer Webseitencontent erzeugt , der mittels Webserver ausgeliefert werden kann.

---
## Skizze SSG
* Markdown
* Bilder
* Javascript
* Build Prozess => Webseite

---
## JAM Stack
* Javascript
* API
* Markup Language

---
## Eigenschaften
* schnell weil Statischer Content
* Durchaus komplexe Webseiten Möglich (Suche)
* keine echten Dynamsichen Inhalte Möglich (Read Only)
* bekannte Prozesse von der Softwareentwicklung (Build / Versionskontrollsysteme)
* Plattform / Stack bleibt einfach
* Security

---
## Stistik SSG
* Auslieferung Dynamische Seiten
* Auslieferung Statische Seiten

---
## SSG Beispiele
* Hugo (https://hugo.io
* Jekyll (https://jekyllrb.com/)
* Pelikan (https://blog.getpelican.com/)

---
## Testumgebung
* Browser http://localhost/index.html

---
## Deployment
* rsync
* sftp

---
## Produktivumgebung
#### Plattform / Stack

| Webserver | Apache |
| --- | --- |
| Anwendung/Sourcecode | index.html,Javascript.js |
| Betriebssystem | Linux |
| Infrastruktur | VM |

---
## Test / Abnahme
* Browser http://produktiv.sam.at/index.html

---
## Management
* Wasserfallmodell

---
## Verbesserungen
* (mikro)dynamische Inhalte
* Datenbankanwendungen

---
## Webapplikation (Dynamisch)

#### Wenn eine Webapplikation nicht nur Inhalt darstellen soll , sondern auch mit dem Benutzer interagiert werden muss , wird eine Dynamische Webapplikation benötigt.

---
## Webapplikation Server

#### Als Webapplikationserver wird jener Teil bezeichnet der die Applikation in der jeweiligen Sprache Interpretiert.

---
## Webapplikation Server

* CGI / FCGI
* Webserver Modul
* Webserver Libary

---
## CGI / FCGI
#### (Fast) Comon Gateway Interface
###### Spezifikation einer Schnittstelle zwischen Webserver und Applikationsserver


---
## CGI / FCGI

| Webserver |
| --- |
| CGI / FCGI|
| HTML |
| Applikation |

---
## Eigenschaften

* Nicht Performant
* Spezifizierte Schnittstelle
* Probleme bei Skalierung


---
## Webserver Modul
#### mod_php
###### Modul des Webservers um die Applikation direkt Aufrufen zu können.

---
## Webserver Modul

| Webserver |
| --- |
| mod_php |
| HTML |
| Applikation |

---
## Eigenschaften


---
## Webserver Libary
#### Ruby / Go
###### Einfach zu benutzende Libary in der Applikationssprache um die HTTP Kommunikation direkt zu machen.

---
## Webserver Libary

| Webserverlibary |
| HTMLLibary |
| Applikationsroutine |


---
## Eigenschaften
* Performant
* Optimiert
* eventuell komplexe config
* Features ?

---
## Deployment Stack
#### LAMP

| Webserver | Apache |
| Anwendung/Sourcecode | ```git clone https:\/\/github.com/kanboard/kanboard``` |
| Applikationsserver | PHP |
| Nutzerdaten | SQLite3 |
| Betriebssystem | Linux |
| Infrastruktur | VM |

---
## Deployment Stack
#### LEMP

| Webserver | Ngnix |
| Anwendung/Sourcecode | ```git clone https:\/\/github.com/kanboard/kanboard``` |
| Applikationsserver | PHP |
| Nutzerdaten | SQLite3 |
| Betriebssystem | Linux |
| Infrastruktur | VM |


---
## Deployment Stack
#### MEAN Stack

| Webserver | Ngnix |
| Anwendung/Sourcecode | ```git clone https:\/\/github.com/kanboard/kanboard``` |
| Applikationsserver | NPM (Express.js/ Angular.js)|
| Nutzerdaten | MongoDB |
| Betriebssystem | Linux |
| Infrastruktur | VM |


---
## Deployment Prozess/Tools
* Deployment
* Abnahme / Testen
* Change Zeiten
* Management Modell

---
## Deployment Skizze

---
## Deployment Beispiel

---
## Deployment Summary

---
## Verbesserung Environments / Umgebungen

Um nicht alle Änderungen / Verbesserungen der Anwendung direkt am Kunden auszutesten . Können für verschiedene Aufgaben / Ziele verschiedene Versionen der Anwendung Ausgerollt werden.

---
## Eigenschaften

* Gekapselt / Unabhängig
* so nahe an der Produktivumgebung wie möglich

---
## Beispiele:
* Development
* Testing
* Staging
* Produktion

---
## Behandlung von Umgebungen in Versionskontrollsystemen

Tag Environments as Brunches in Configuration Management


---
### Verbesserungen
* Skalierung Horizontal zu Vertikal

---
## Horizontale Skalierung
#### Bei der horizontalen Skalierung werden bei engpässen in der Performance einfach die Resourcen der Hardware / VM hochgeschraubt.

---
## Beispiel:
200 User
4 GB RAM
1 vCPU

---
## Beispiel:
400 User
8 GB RAM
2 vCPU

---
* gut geeignet für Monolithische Applikationen
* Performance steigt leider nicht Linear




---
## Verbesserungen
* Trennung von Statischen und Dynamischen Inhalten

---
## Vorteile
* Performance
* Skalierung

---
## Nachteile
* Komplexität steigt

---
## Konzept Content Delivery Netzwerk

Um bessere Performance und Latenzen zu erreichen werden Statische und Dynamische Inhalte getrennt und die Statischen Daten über eine getrennte und meist Weltweit verteilte Infrastruktur genannt Content Delivery Netzwerk ausgeliefert.

---
## Eigenschaften Content Delivery Netzwerk
* schnelle Auslieferung von statischem Content
* Weltweit verteilte Infrastruktur

---
## Content
* Bilder
* Statisches Javascript
* Software Updates

---
## Zusätzliche Funktionen
* Caching von Inhalten
* DDOS Protection
* DNS Routing (Global Loadbalancer)

---
## Beispiele CDN
* Varnish
* Akamai (https://www.akamai.com)
* Cloudflare (https://www.cloudflare.com)

---
## Content Delivery Netzwerk
Beispiel:
* 2 x Loadbalancer
* Content Delivery Netzwerk
* 2 x Webserver ( Statecache Redis)
* Datenbank

---
## Skizze CDN

---
## Skizze Caching von Dynamischen Inhalten



---
## Verbesserung
* Trennung des Applikationsstacks

---
Beispiel:
* 1 x Webserver
* 1 x Datenbank

---
## Vorteile

* Performance
* Skalierung
* höhere Verfügbarkeit
* flexible bei Updates

---
## Beispiel

* 2 x Webserver
* 1 x Datenbank

---
## Konzept Loadbalancer
* Um die Lasten auf die Applikationsserver zu verteilen und dadurch eine bessere Performance zu erreichen
* es werden verschiedene Methoden verwendet um dies zu erreichen
* OSI 7 Layer Modell

Beispiel:
* 2 x Loadbalancer (Stateless)
* 2 x Webserver
* 1 x Datenbank

---
## Monolithic Application

---
## Konzept Microservices
* Sollte überschaubar sein
* kleine gekapselte Services
* Grundregel in 1 bis 2 Wochen ersetzbar
* Aus der Software Entwicklung
* UNIX Prinzip kleine Unabhänginge Programme / Prozesse / Services die miteinander ein großes ganzes ergeben

---
## Microservices
Beispiel:
* 2 x Loadbalancer (Stateless)
* 2 x Webserver (Statecache Redis)
* 1 x Datenbank


---
## Konzept Stateless Application
* Trennt die Daten von der Ausführungsebene / Applikation somit wird ein Austausch der Applikation (Versionsupdate) einfacher
* Einfaches Deployment
* Hoher Automatisierungsgrad
* Hoher Standartisierungsgrad
* Docker / Rocket / LXD
* Docker Konzept Build Ship Run

---
## Webapplikation / Architektur (Summary)
* HTML
* Javascript
* Bilder
* Dynamischer Inhalt (Anwendung)
* Persistente Daten (Datenbank)


---
## Deployments
* A/B Deployment
* Canary Deployment
* Rolling Releases

---
### Konzept Cannary Deployment
Staging und Produktion Umgebungen wachsen immer mehr zusammen. Ein kleiner Bereich wird mit der neuen Version des Codes Bespielt und getestet geht alles glatt wird diese Version in die Produktion Übernommen.

* Automatisiertes Deployment
* Automatisierte Tests

---
## Konzept Rolling Release
Staging und Produktion Umgebungen werden über den Loadbalancer Kontrolliert und Automtisch Deployed.

![](_images/rolling-release.png)



---
## Testing
* A/B
* Blue/ Green
* Unit Tests
* Integration Tests
* Smoke Tests

---
## Continious Integration

---
## Continious Deployment

---
## Gitops 1

![](_images/gitops-1.png)

---
## Gitops 2

![](_images/gitops-2.png)

---
## Gitops 3

![](_images/gitops-3.png)

---
## Gitops 4

![](_images/gitops-4.png)

---
## Gitops 5

![](_images/gitops-5.png)


---
# Links
* https://hackernoon.com/static-vs-dynamic-website-whats-the-difference-in-2019-327064bfd4a9
* Cisco Live 2017 Devops Wasserfallmodell
* Technologieplauscherl 56 Donewan Brown
* Technologieplauscherl - 2 Projektmanagement (Ziele Festlegen / Navigationssystem)
* Arge Storage 2017 PETs Cows Chicken
* Sam Newman (Microservices-Konzeption und Design)
  ISBN: 978-3-95845-083-7
* Martin Fowler ???
* GIT
